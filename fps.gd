extends CanvasLayer

@onready var last_frames_drawn = Engine.get_frames_drawn()
@onready var last_check_time = Time.get_ticks_usec()

func _process(delta):
	var time = Time.get_ticks_usec()
	var time_delta = time - last_check_time
	if time_delta > 1000000:
		var frames_drawn = Engine.get_frames_drawn()
		var frames_drawn_delta = frames_drawn - last_frames_drawn
		var fps = frames_drawn_delta / (time_delta / 1000000.0)
		$Label.text = '%.1f' % fps
		print('setting')
		last_frames_drawn += frames_drawn_delta
		last_check_time += time_delta
