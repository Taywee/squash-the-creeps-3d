class_name Mob extends CharacterBody3D

@export var min_speed = 10.0
@export var max_speed = 18.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _physics_process(_delta):
	move_and_slide()

func initialize(start_position: Vector3, player_position: Vector3):
	look_at_from_position(start_position, player_position, Vector3.UP)
	rotate_y(randf_range(-TAU / 16, TAU / 16))
	var speed = randf_range(min_speed, max_speed)
	velocity = Vector3.FORWARD * speed
	velocity = velocity.rotated(Vector3.UP, rotation.y)

func _on_visibility_notifier_screen_exited():
	queue_free()
