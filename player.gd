extends CharacterBody3D

@export var speed = 14
@export var fall_acceleration = 75
@export var jump_velocity = 17

var fall_speed = 0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")


func _physics_process(_delta):
	var direction = Vector3(
		Input.get_action_strength('move_right') - Input.get_action_strength('move_left'),
		0,
		Input.get_action_strength('move_back') - Input.get_action_strength('move_forward'),
	)

	var normalized_direction = Vector3.ZERO

	if direction != Vector3.ZERO:

		normalized_direction = direction.normalized()

		$Pivot.look_at($Pivot.global_position + normalized_direction, Vector3.UP)

		if direction.length_squared() > normalized_direction.length_squared():
			direction = normalized_direction

	if not is_on_floor():
		fall_speed = fall_speed - (fall_acceleration * _delta)

	velocity = Vector3(
		direction.x * speed,
		fall_speed,
		direction.z * speed,
	)

	move_and_slide()
